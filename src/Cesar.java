import java.util.ArrayList;

public class Cesar 
{
 
    public static String decoder(ArrayList<Character> listeCaracteres, int decalage) 
    {
    		decalage = decalage % 26 + 26;
        StringBuilder encoded = new StringBuilder();
        
        for (char i : listeCaracteres) 
        {
            if (Character.isLetter(i)) 
            {
                if (Character.isUpperCase(i)) 
                {
                    encoded.append((char) ('A' + (i - 'A' + decalage) % 26 ));
                } 
                else 
                {
                    encoded.append((char) ('a' + (i - 'a' + decalage) % 26 ));
                }
            } 
            else 
            {
                encoded.append(i);
            }
        }
        
        return encoded.toString();
    }
}
