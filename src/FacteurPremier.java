import java.util.ArrayList;
import java.util.HashMap;

public class FacteurPremier 
{
	public static String decoder(ArrayList<Character> listeCaracteres)
	{
		ArrayList<Integer> nombres = new ArrayList<Integer>();
		ArrayList<Integer> facteurs = new ArrayList<Integer>();
		HashMap<Integer,Character> alphabet = new HashMap<Integer,Character>();
		
		String nombre = "";
		String texte = "";
		
		for(Character c : listeCaracteres)
		{
			if (c == ' ')
			{
				nombres.add(Integer.parseInt(nombre));
				nombre = "";
			}
			else
			{
				nombre = nombre + String.valueOf(c);
			}
		}
		
		for(Integer n : nombres)
		{
			int diviseur;
			int nbfacteurs;
			int a;
			
			a = n;
			nbfacteurs = 0;
			
			for(diviseur = 2 ; diviseur <= (int)n/2 ; diviseur++)
			{
				if(a % diviseur == 0)
				{
					nbfacteurs++;
					
					while(a % diviseur == 0)
					{
						facteurs.add(diviseur);
						a = a / diviseur;
					}
				}
			}
			
			if(nbfacteurs == 0)
			{
				facteurs.add(n);
			}
		}
		
		alphabet.put(2,'a');
		alphabet.put(3,'b');
		alphabet.put(5,'c');
		alphabet.put(7,'d');
		alphabet.put(11,'e');
		alphabet.put(13,'f');
		alphabet.put(17,'g');
		alphabet.put(19,'h');
		alphabet.put(23,'i');
		alphabet.put(29,'j');
		alphabet.put(31,'k');
		alphabet.put(37,'l');
		alphabet.put(41,'m');
		alphabet.put(43,'n');
		alphabet.put(47,'o');
	    	alphabet.put(53,'p');
	    	alphabet.put(59,'q');
	    	alphabet.put(61,'r');
	    	alphabet.put(67,'s');
	    	alphabet.put(71,'t');
	    	alphabet.put(73,'u');
	    	alphabet.put(79,'v');
	    	alphabet.put(83,'w');
	    	alphabet.put(89,'x');
	    	alphabet.put(97,'y');
	    	alphabet.put(101,'z');
    	
    	for(Integer f : facteurs)
		{
			texte = texte + alphabet.get(f);
		}
		
		return texte;
	}
}
