import java.util.ArrayList;
import java.util.HashMap;

public class Analyseur
{
	public static String analyser(ArrayList<Character> listeCaracteres)
	{
		String texte = "Impossible de décoder le fichier";

		if(decoderUTF8(listeCaracteres))
		{
			System.out.println("Le texte est encodé en UTF8 et voici la traduction :\n");
			texte = UTF8.decoder(listeCaracteres);
		}

		if(decoderUTF16(listeCaracteres))
		{
			System.out.println("Le texte est encodé en UTF16 et voici la traduction :\n");
			texte = UTF16.decoder(listeCaracteres);
		}

		if(decoderUnicode(listeCaracteres))
		{
			System.out.println("Le texte est encodé en Unicode et voici la traduction :\n");
			texte = Unicode.decoder(listeCaracteres);
		}

		if(decoderBase64(listeCaracteres))
		{
			Base64.decoder(listeCaracteres);
			System.out.println("Le texte est encodé en Base 64 et voici la traduction :\n");
			texte = "C'est une image enregistrée en image.png";
		}

		if(decoderNombrePremier(listeCaracteres))
		{
			System.out.println("Le texte est encodé en nombre premier et voici la traduction :\n");
			texte = NombrePremier.decoder(listeCaracteres);
		}
		//Un facteur peut être un nombre premier --> évite un double message
		else if(decoderFacteurPremier(listeCaracteres))
		{
			System.out.println("Le texte est encodé en facteur premier et voici la traduction :\n");
			texte = FacteurPremier.decoder(listeCaracteres);
		}

		if(decoderSansChiffre(listeCaracteres))
		{
			System.out.println("Le texte est encodé avec des chiffres en trop et voici la traduction :\n");
			texte = EnleverInt.decoder(listeCaracteres);
		}

		if(decoderVigenereCesar(listeCaracteres))
		{
			System.out.println("Le texte est encodé en Vigenere ou César\n");
			texte = Vigenere.decoder(listeCaracteres);
		}

		return texte+"\n";
	}

	public static boolean decoderUTF8(ArrayList<Character> listeCaracteres)
	{
		boolean decoderUTF8 = true;
		int pas = 0;

		HashMap<Character,Integer> tableHexa = new HashMap<Character,Integer>();
		tableHexa.put('0', 0);
		tableHexa.put('1', 1);
		tableHexa.put('2', 2);
		tableHexa.put('3', 3);
		tableHexa.put('4', 4);
		tableHexa.put('5', 5);
		tableHexa.put('6', 6);
		tableHexa.put('7', 7);
		tableHexa.put('8', 8);
		tableHexa.put('9', 9);
		tableHexa.put('A', 10);
		tableHexa.put('B', 11);
		tableHexa.put('C', 12);
		tableHexa.put('D', 13);
		tableHexa.put('E', 14);
		tableHexa.put('F', 15);

		for(int i = 0; i<listeCaracteres.size() && decoderUTF8 ; i++)
		{
			char c = listeCaracteres.get(i);
			if(!tableHexa.containsKey(c) && c != ' ')
			{
				decoderUTF8 = false;
			}
		}

			for(int i = 0; i<listeCaracteres.size() && decoderUTF8 ; i++)
			{
				char c = listeCaracteres.get(i);

				if(c != ' ')
				{
					pas++;
				}
				else
				{
					//Si les nombres ne sont pas encodés sur 2 octets
					if(pas != 2)
					{
						decoderUTF8 = false;
					}
					pas = 0;
				}
			}

		return decoderUTF8;
	}

	public static boolean decoderUTF16(ArrayList<Character> listeCaracteres)
	{
		boolean decoderUTF16 = true;
		int pas = 0;

		HashMap<Character,Integer> tableHexa = new HashMap<Character,Integer>();
		tableHexa.put('0', 0);
		tableHexa.put('1', 1);
		tableHexa.put('2', 2);
		tableHexa.put('3', 3);
		tableHexa.put('4', 4);
		tableHexa.put('5', 5);
		tableHexa.put('6', 6);
		tableHexa.put('7', 7);
		tableHexa.put('8', 8);
		tableHexa.put('9', 9);
		tableHexa.put('A', 10);
		tableHexa.put('B', 11);
		tableHexa.put('C', 12);
		tableHexa.put('D', 13);
		tableHexa.put('E', 14);
		tableHexa.put('F', 15);

		for(int i = 0; i<listeCaracteres.size() && decoderUTF16 ; i++)
		{
			char c = listeCaracteres.get(i);
			if(!tableHexa.containsKey(c) && c != ' ')
			{
				decoderUTF16 = false;
			}
		}

			for(int i = 0; i<listeCaracteres.size() && decoderUTF16 ; i++)
			{
				char c = listeCaracteres.get(i);

				if(c != ' ')
				{
					pas++;
				}
				else
				{
					//Si les nombres ne sont pas encodés sur 4 octets
					if(pas != 4)
					{
						decoderUTF16 = false;
					}
					pas = 0;
				}
			}

		return decoderUTF16;
	}

	public static boolean decoderUnicode(ArrayList<Character> listeCaracteres)
	{
		boolean decoderUnicode = true;
		boolean espace = false;
		String nombre = "";

		for(int i = 0; i<listeCaracteres.size() && decoderUnicode ; i++)
		{
			char c = listeCaracteres.get(i);

			if(!Character.isDigit(c) && c != ' ')
			{
				decoderUnicode = false;
			}
		}

		for(int i = 0; i<listeCaracteres.size() && decoderUnicode ; i++)
		{
			char c = listeCaracteres.get(i);

			if(c != ' ')
			{
				nombre = nombre + String.valueOf(c);
			}
			else
			{
				//Si le texte contient au moins un espace
				if(nombre.equals("32"))
				{
					espace = true;
				}
				nombre = "";
			}
		}

		decoderUnicode = espace;

		return decoderUnicode;
	}

	public static boolean decoderBase64(ArrayList<Character> listeCaracteres)
	{
		boolean decoderBase64 = true;
		HashMap<Character,Integer> occurenceLettre = new HashMap<Character,Integer>();
		int proba = 0;

		for(int i = 0; i<listeCaracteres.size() && decoderBase64 ; i++)
		{
			char c = listeCaracteres.get(i);
			if(occurenceLettre.containsKey(c))
			{
				int occurence = occurenceLettre.get(c);
				occurence++;
				occurenceLettre.put(c, occurence);
			}
			else occurenceLettre.put(c, 1);
		}

		if(occurenceLettre.containsKey('/') && occurenceLettre.containsKey('='))
		{
			proba = listeCaracteres.size() / occurenceLettre.get('/');
			if(proba > 100) decoderBase64 = false;
		}
		else decoderBase64 = false;


		return decoderBase64;
	}

	public static boolean decoderNombrePremier(ArrayList<Character> listeCaracteres)
	{
		boolean decoderNombrePremier = true;
		String nombre = "";

		for(int i = 0; i<listeCaracteres.size() && decoderNombrePremier ; i++)
		{
			char c = listeCaracteres.get(i);

			if(!Character.isDigit(c) && c != ' ')
			{
				decoderNombrePremier = false;
			}
		}

		for(int i = 0; i<listeCaracteres.size() && decoderNombrePremier ; i++)
		{
			char c = listeCaracteres.get(i);

			if(c != ' ')
			{
				nombre = nombre + String.valueOf(c);
			}
			else
			{
				int n = Integer.valueOf(nombre);

				if(n<=1) decoderNombrePremier = false;
				else
				{
			        for(int j = 2 ; j*j <= n; j++)
			        {
			            if (n%j == 0) decoderNombrePremier = false;
			            j++;
			        }
				}

				nombre = "";
			}
		}

		return decoderNombrePremier;
	}

	public static boolean decoderSansChiffre(ArrayList<Character> listeCaracteres)
	{
		boolean decoderSansChiffre = true;
		boolean estChiffre = false;
		boolean estLettre = false;
		boolean espace = false;

		for(int i = 0; i<listeCaracteres.size() && !espace ; i++)
		{
			char c = listeCaracteres.get(i);
			if(c == ' ')
			{
				espace = true;
			}
		}

		decoderSansChiffre = espace;

		for(int i = 0; i<listeCaracteres.size() && decoderSansChiffre ; i++)
		{
			char c = listeCaracteres.get(i);

			if(c != ' ')
			{
				if(Character.isDigit(c))
				{
					estChiffre = true;
				}
				else estLettre = true;
			}
			else
			{
				if(!estChiffre || !estLettre)
				{
					decoderSansChiffre = false;
				}
				estChiffre = false;
				estLettre = false;
			}
		}

		return decoderSansChiffre;
	}

	public static boolean decoderFacteurPremier(ArrayList<Character> listeCaracteres)
	{
		boolean decoderFacteurPremier = true;
		String nombre = "";
		ArrayList<Character> facteurs = new ArrayList<Character>();

		for(int i = 0; i<listeCaracteres.size() && decoderFacteurPremier ; i++)
		{
			char c = listeCaracteres.get(i);

			if(!Character.isDigit(c) && c != ' ')
			{
				decoderFacteurPremier = false;
			}
		}

		for(int i = 0 ; i<listeCaracteres.size() && decoderFacteurPremier ; i++)
		{
			char c = listeCaracteres.get(i);

			if(c != ' ')
			{
				nombre = nombre + String.valueOf(c);
			}
			else
			{
				if(nombre.equals("32"))
				{
					decoderFacteurPremier = false;
				}
				else
				{
					int diviseur;
					int nbfacteurs;
					int a;

					a = Integer.parseInt(nombre);
					nbfacteurs = 0;

					for(diviseur = 2 ; diviseur <= (int)(Integer.parseInt(nombre)/2) ; diviseur++)
					{
						if(a % diviseur == 0)
						{
							nbfacteurs++;

							while(a % diviseur == 0)
							{
								char[] chars = String.valueOf(diviseur+" ").toCharArray();
								for(int j = 0 ; j<chars.length ; j++)
								{
									facteurs.add(chars[j]);
								}
								a = a / diviseur;
							}
						}
					}

					if(nbfacteurs == 0)
					{
						nombre = nombre + " ";
						char[] chars = nombre.toCharArray();
						for(int j = 0 ; j<chars.length ; j++)
						{
							facteurs.add(chars[j]);
						}
					}

					nombre = "";
				}
			}
		}

		if(decoderFacteurPremier)
		{
			decoderFacteurPremier = decoderNombrePremier(facteurs);
		}

		return decoderFacteurPremier;
	}

	public static boolean decoderVigenereCesar(ArrayList<Character> listeCaracteres)
	{
		boolean decoderVigenereCesar = true;

		for(int i = 0; i<listeCaracteres.size() && decoderVigenereCesar ; i++)
		{
			char c = Character.toLowerCase(listeCaracteres.get(i));

			if(Character.isDigit(c) || c == '/')
			{
				decoderVigenereCesar = false;
			}
		}

		return decoderVigenereCesar;
	}
}
