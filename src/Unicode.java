import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class Unicode 
{
	public static String decoder(ArrayList<Character> listeCaractere)
	{
		byte[] octets = new byte[listeCaractere.size()*2];
		String nombre = "";
		int k = 0;

		for(char c : listeCaractere)
		{
			if(c != ' ')
			{
				nombre = nombre + String.valueOf(c);
			}
			else
			{
				octets[k] = 0;
				octets[k+1] = 0;
				octets[k+2] = 0;
				octets[k+3] = (byte)Integer.valueOf(nombre).intValue();
				k = k + 4;
				nombre = "";
			}
		}
		
		return new String(octets, StandardCharsets.UTF_16);
	}
}
