import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;

import sun.misc.*;

import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Base64 {
        
        public static void decoder(ArrayList<Character> listeCaracteres) 
        {
        		String imageString = "";
        		
        		for(char c : listeCaracteres)
        		{
        			imageString = imageString + String.valueOf(c);
        		}
                 
                // create a buffered image
                BufferedImage image;
                byte[] imageByte;
                BASE64Decoder decoder;
                ByteArrayInputStream bis;
                File outputfile;
                
                try
                {
                    decoder = new BASE64Decoder();
                    imageByte = decoder.decodeBuffer(imageString);
                    bis = new ByteArrayInputStream(imageByte);
                    outputfile = new File("image.png");
                    image = ImageIO.read(bis);
                    ImageIO.write(image, "png", outputfile);
                    bis.close();
                }
                catch(Exception e)
                {
                        
                }
        }

}
