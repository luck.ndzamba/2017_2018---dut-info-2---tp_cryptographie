# 2017_2018 - DUT INFO 2 - TP_Cryptographie

Programme Java qui décode automatiquement les formats suivants :
- UTF8
- UTF16
- Unicode
- Base 64
- Les nombres premiers
- Les facteurs premiers
- Les chiffres en trop
- Vigenere
- César

<img width="500" height="400" src="Cryptographie.jpg">